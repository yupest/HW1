int clockPin = 13;
int latchPin = 10;
int dataPin = 12;
//объявление всех встречающихся символов, которые индикатор может вывести
byte off = 0b0000000;//для паузы в словах (и между буквами)
byte a = 0b1110111;
byte b = 0b1001111;
byte B = 0b1111111;
byte C = 0b0011101;
byte d = 0b1101110;
byte e = 0b0011111;
byte f = 0b0010111;
byte G = 0b1011111;
byte g = 0b1111011;
byte h = 0b1000111;
byte H = 0b1100111;
byte I = 0b1100000;
byte j = 0b1101000;
byte l = 0b0001101;
byte n = 0b1000110;
byte o = 0b1111101;
byte p = 0b0110111;
byte q = 0b1110011;
byte r = 0b0000110;
byte s = 0b1011011;
byte t = 0b0001111;
byte u = 0b1101101;
byte y = 0b1101011;
byte two = 0b0111110;
byte three = 0b1111010;
byte four = 0b1100011;
byte seven = 0b1110000;
byte dash = 0b0000010;
bool check = false;//для перехода на новую строку после слова
void setup() {
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
  pinMode(latchPin, OUTPUT);
  Serial.begin(9600);
}
void loop() {
  char val = Serial.read();
  if (Serial.available() > 0) {
    Serial.print(val);
    check = true;
  }
  chooseLetter(val);//функция для подбора необходимого символа
}
void readWord(byte symbol) {//функция отображаюшая на 7сегментном индикаторе необходимый символ 
  digitalWrite(latchPin, HIGH);
  shiftOut(dataPin, clockPin, LSBFIRST, symbol);
  digitalWrite(latchPin, LOW);
  delay(500);
}
void chooseLetter(char value) {
  readWord(off);
  switch (value) {
    case  'A':
      readWord(a);
      break;
    case 'a' :
      readWord(a);
      break;
    case 'b':
      readWord(b);
      break;
    case 'B':
      readWord(B);
      break;
    case '8':
      readWord(B);
      break;
    case 'c':
      readWord(C);
      break;
    case 'C' :
      readWord(C);
      break;
    case 'd':
      readWord(d);
      break;
    case  'D':
      readWord(d);
      break;
    case 'e' :
      readWord(e);
      break;
    case  'E':
      readWord(e);
      break;
    case 'f' :
      readWord(f);
      break;
    case 'F':
      readWord(f);
      break;
    case 'g' :
      readWord(g);
      break;
    case '9' :
      readWord(g);
      break;
    case 'G':
      readWord(G);
      break;
    case '6':
      readWord(G);
      break;
    case 'h':
      readWord(h);
      break;
    case 'H':
      readWord(H);
      break;
    case 'i' :
      readWord(I);
      break;
    case '1' :
      readWord(I);
      break;
    case  'I':
      readWord(I);
      break;
    case 'j' :
      readWord(j);
      break;
    case 'J':
      readWord(j);
      break;
    case 'l' :
      readWord(l);
      break;
    case 'L':
      readWord(l);
      break;
    case 'n':
      readWord(n);
      break;
    case 'o' :
      readWord(o);
      break;
    case  'O':
      readWord(o);
      break;
    case  '0':
      readWord(o);
      break;
    case 'p':
      readWord(p);
      break;
    case 'P':
      readWord(p);
      break;
    case 'q':
      readWord(q);
      break;
    case 'Q':
      readWord(q);
      break;
    case  'R':
      readWord(r);
      break;
    case 'r' :
      readWord(r);
      break;
    case 'S':
      readWord(s);
      break;
    case 's' :
      readWord(s);
      break;
    case '5' :
      readWord(s);
      break;
    case  'T':
      readWord(t);
      break;
    case 't' :
      readWord(t);
      break;
    case 'u':
      readWord(u);
      break;
    case 'U':
      readWord(u);
      break;
    case 'y':
      readWord(y);
      break;
    case 'Y':
      readWord(y);
      break;
    case '-':
      readWord(dash);
      break;
    case '2':
      readWord(two);
      break;
    case '3':
      readWord(three);
      break;
    case '4':
      readWord(four);
      break;
    case '7':
      readWord(seven);
      break;
    default:
      readWord(off);
      if (check) {
        Serial.println();
        check = false;
      }
  }
}

